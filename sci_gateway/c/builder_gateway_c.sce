//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2008 - DIGITEO - Vincent COUVERT
//  Copyright (C) 2010-2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function builder_gw()
  currentpath = get_absolute_file_path("builder_gateway_c.sce");
  gw_name = "xmltodocbook";
  gw_table = ["XMLConvert5", "sci_xmltodocbook"];
  gw_files = ["sci_xmltodocbook.c"];
  
  INCLUDES_PATHS = "-I" + fullpath(currentpath + "../../includes/");
  if getos() <> "Windows" then 
    // Source tree version
    if isdir(SCI+"/modules/core/includes/") then
      INCLUDES_PATHS = INCLUDES_PATHS + " -I" + SCI + "/modules/localization/includes/";
    end
  
    // Binary version
    if isdir(SCI+"/../../include/scilab/core/") then
      INCLUDES_PATHS = INCLUDES_PATHS + " -I" + SCI + "/../../include/scilab/localization/";
    end
  
    // System version (ie: /usr/include/scilab/)	
    if isdir("/usr/include/scilab/") then
      INCLUDES_PATHS = INCLUDES_PATHS + " -I/usr/include/scilab/localization/";
    end    
  end
  tbx_build_gateway(gw_name , gw_table, gw_files, currentpath,"../../src/jni/libxmltodocbook", "", INCLUDES_PATHS);
  
endfunction

builder_gw();
clear builder_gw;
