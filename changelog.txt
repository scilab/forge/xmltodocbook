changelog of the xmltodocbook toolbox

xmltodocbook (1.2)
    * ticket 75 fixed - xmltodocbook will not start in -NWNI mode
    * ticket 414 fixed - xmltodocbook made Scilab unstable on Windows
    * ticket 415 fixed - build was required directly in toolbox directory.
 -- Allan CORNET - DIGITEO


xmltodocbook (1.1)
    * Updated for Scilab 5.3
    * test added
 -- Allan CORNET - DIGITEO


xmltodocbook (1.0)
    * Initial version
 -- Vincent COUVERT <vincent.couvert@scilab.org>
 -- Sylvestre LEDRU <sylvestre.ledru@scilab.org>
