/*
 *  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 *  Copyright (C) 2008 - INRIA - Sylvestre LEDRU
 *  Copyright (C) 2008 - INRIA - Vincent COUVERT
 *
 *  This file must be used under the terms of the CeCILL.
 *  This source file is licensed as described in the file COPYING, which
 *  you should have received as part of this distribution.  The terms
 *  are also available at
 *  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

/*--------------------------------------------------------------------------*/
#include "XMLConvert5.hxx"
#include "GiwsException.hxx"

#include <string.h>
extern "C"
{
#include "Scierror.h"
#include "getScilabJavaVM.h"
#include "localization.h"
#include "BOOL.h"

  /*--------------------------------------------------------------------------*/
#ifdef _MSC_VER
#define DLLEXPORT_XMLTODOCBOOK __declspec(dllexport)
#else
#define DLLEXPORT_XMLTODOCBOOK
#endif
  /*--------------------------------------------------------------------------*/
#ifdef _MSC_VER
  static void __slashToAntislash(char *in)
  {
  	std::string strin(in);
    size_t found = strin.rfind("/");

    while (found != std::string::npos)
      {
        strin.replace (found, 1, "\\");
        found = strin.rfind("/");
      }
     in = strdup(strin.c_str());
  }
#endif
  /*--------------------------------------------------------------------------*/
  DLLEXPORT_XMLTODOCBOOK int xmltodocbook(char *sourceXML,  char *destXML)
  {
  
    org_scilab_toolboxes_xmltodocbook::XMLConvert5 *convert = NULL;
  
    try
      {
        convert = new org_scilab_toolboxes_xmltodocbook::XMLConvert5(getScilabJavaVM());
      
#ifdef _MSC_VER
        __slashToAntislash(sourceXML);
        __slashToAntislash(destXML);
#endif
      
        convert->process(sourceXML, destXML);
      }
    catch(GiwsException::JniException ex)
      {
        Scierror(999,_("%s: Error while converting the documentation file %s: %s.\n"), "xmltodocbook", sourceXML, ex.getJavaDescription().c_str());
        return FALSE;
      }
  
    if (convert != NULL)
      {
        delete convert;
      }
  	return TRUE;
  }
  /*--------------------------------------------------------------------------*/
}
