/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA - Sylvestre LEDRU
 * Copyright (C) 2008 - DIGITEO - Vincent COUVERT
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.toolboxes.xmltodocbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.icl.saxon.StyleSheet; /* saxon */

public class XMLConvert5 extends StyleSheet {

	private static final String SCI = System.getenv("SCI");
	
	private static final String ERROR_READ = "Could not load file: ";
	private static final String ERROR_WRITE = "Could not save file: ";
	
	public void process(String sourceXML, String destXML) throws FileNotFoundException {

		/* Check if file exists */
		if (!new File(sourceXML).isFile()) {
			throw new FileNotFoundException("Could not find source document: " + sourceXML);
		}
		
		/*******************************/
		/* Remove reference to old DTD */
		/*******************************/
		Document document = null;
		File xml = null;
		DocumentBuilder docBuilder = null;
		
		/* Load source file */
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			/* Do not try to load old DTD because it does not exist anymore */
			factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			docBuilder = factory.newDocumentBuilder();
			xml = new File(sourceXML);
			document = docBuilder.parse(xml);
		} catch (ParserConfigurationException pce) {
			throw new FileNotFoundException(ERROR_READ + sourceXML);
		} catch (SAXException se) {
			throw new FileNotFoundException(ERROR_READ + sourceXML);
		} catch (IOException ioe) {
			throw new FileNotFoundException(ERROR_READ + sourceXML);
		}

		/* Modify contents */
		Element root = document.getDocumentElement();
		NodeList doctypes = root.getElementsByTagName("doctype");
		if (doctypes.getLength() != 0) {
			Element doctype = (Element) doctypes.item(0);
			root.removeChild(doctype);
		}
		
		/* Save source file */
		File newSourceXML = null;
		try {
			newSourceXML = File.createTempFile("xmltodocbook", null);
		} catch (IOException e) {
			throw new FileNotFoundException("Could not create temporary file.");
		}
		Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException e1) {
			throw new FileNotFoundException(ERROR_WRITE + newSourceXML.getAbsolutePath());
		} catch (TransformerFactoryConfigurationError e1) {
			throw new FileNotFoundException(ERROR_WRITE + newSourceXML.getAbsolutePath());
		}
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		StreamResult result = new StreamResult(newSourceXML);
		DOMSource source = new DOMSource(document);
		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			throw new FileNotFoundException(ERROR_WRITE + newSourceXML.getAbsolutePath());
		}
		
		/****************/
		/* Convert file */
		/****************/
		ArrayList<String> args = new ArrayList<String>();
		args.add("-o");
		args.add(destXML);
		args.add(newSourceXML.getAbsolutePath());
		args.add(SCI + "/modules/helptools/xsl/convert/manrev2sci.xsl");
		doMain(args.toArray(new String [args.size()]), this, "java com.icl.saxon.StyleSheet");
		
		/* Remove temporary file */
		newSourceXML.delete();
		
		/* Check if output file has been generated */
		if (!new File(destXML).isFile()) {
			throw new FileNotFoundException("An error occurred during the conversion process. Could not find output document: " + destXML);
		}

	}
}
